# 6.19.2015 V1
* Added command to view the currently running version and changelog
* Fixed incorrectly configured thread
* Added command to shutdown bot