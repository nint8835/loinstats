version = raw_input("Version: ")

with open(version + ".md", "w") as f:
    f.write("# " + version + "\n")
    
    writing = True
    while writing:
        change = raw_input("Change (or exit to stop): ")
        if change == "exit":
            writing = False
            
        else:
            f.write("* " + change + "\n")